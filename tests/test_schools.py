def test_fetch_schools(client):
	rv = client.get("/colleges/1/schools")

	assert rv.status_code == 200
	assert b"schools" in rv.data
