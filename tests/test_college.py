def test_fetch_colleges(client):
	rv = client.get("/colleges")

	assert rv.status_code == 200
	assert b"colleges" in rv.data
