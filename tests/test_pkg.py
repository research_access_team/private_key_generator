from pkg.core.pkg import PKG
from .cryptosystem import encrypt_using_rsa, decrypt


def test_get_private_key():
	pkg = PKG("rsa")

	test_id = "test_email"
	private_key = pkg.get_private_key(test_id)

	assert type(private_key) == int


def test_get_public_key():
	pkg = PKG("rsa")

	test_id = "test_email"
	public_key = pkg.get_public_key_based_on_id(test_id)

	assert type(public_key) == int


def test_encryption_and_decryption():
	pkg = PKG("rsa")

	test_id = "test_email"
	test_message = "t" * 256

	public_key = pkg.get_public_key_based_on_id(test_id)
	public_params = pkg.get_public_params()

	cipher_text = encrypt_using_rsa(
		test_message, public_key, public_params)

	private_key = pkg.get_private_key(test_id)
	plain_text = decrypt(cipher_text, private_key, public_params,"rsa")

	assert plain_text == bytes(test_message, "utf-8")
	
