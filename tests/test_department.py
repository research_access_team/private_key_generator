def test_fetch_departments(client):
	rv = client.get("/schools/1/departments")

	assert rv.status_code == 200
	assert b"departments" in rv.data
