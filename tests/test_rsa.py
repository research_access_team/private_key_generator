from pkg.core.rsa import RSA


def test_is_prime():
	assert RSA.is_prime(37, 1000)
	assert not RSA.is_prime(4, 1000)


def test_generate_big_prime():
	big_prime = RSA.generate_big_prime(512)
	assert type(big_prime) == int
