import json

from pkg.models.researcher import Researcher


def test_registration_empty_name(client):
	fields = {
		"name": "",
		"email": "test_email",
		"password": "test_password",
		"department_id": 1
	}

	rv = send_json_post(client, "/researchers", fields)
	assert rv.status_code == 400


def test_registration_empty_email(client):
	fields = {
		"name": "test_name",
		"email": "",
		"password": "test_password",
		"department_id": 1
	}
	
	rv = send_json_post(client, "/researchers", fields)
	assert rv.status_code == 400


def test_registration_empty_password(client):
	fields = {
		"name": "test_name",
		"email": "test_email",
		"password": "",
		"department_id": 1
	}
	
	rv = send_json_post(client, "/researchers", fields)
	assert rv.status_code == 400


def test_registration_empty_department(client):
	fields = {
		"name": "test_name",
		"email": "test_email",
		"password": "test_password",
		"department_id": None
	}
	
	rv = send_json_post(client, "/researchers", fields)
	assert rv.status_code == 400


def test_registration_full_details(app, client):
	fields = {
		"name": "test_name",
		"email": "test_email",
		"password": "test_password",
		"department_id": 1
	}
	
	rv = send_json_post(client, "/researchers", fields)

	# Test response code
	assert rv.status_code == 201
	assert b"token" in rv.data

	# Test if researcher was actually created
	with app.app_context():
		test_researcher = Researcher.query.filter_by(
			email=fields["email"]).first()

	assert test_researcher is not None
	assert test_researcher.name == fields["name"]


def send_json_post(client, endpoint: str, data):
	"""
		Package a list of keyword args into JSON and send them as a POST
		test request
		:param client:
		:param endpoint:
		:param kwargs:
		:return:
	"""
	return client.post(
		endpoint,
		data=json.dumps(data),
		content_type="application/json"
	)
