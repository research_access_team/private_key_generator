from setuptools import setup, find_packages


requirements_file = open("requirements.txt")
# Remove newline characters in the list of requirements
requirements = [requirement.strip() for requirement in \
				requirements_file.readlines()]

setup(
	name="pkg-api",
	version="1.0.0",
	description="Private Key Generator API that provides Identity Base Encryption\
	to REST compliant clients",
	long_description=open("README.md").read(),
	packages=find_packages(),
	include_package_data=True,
	install_requires=requirements
)
