import logging
from logging import handlers

from werkzeug.security import generate_password_hash, check_password_hash

from pkg import constants


def non_empty_string(s):
	if not s:
		raise ValueError("Expected a non empty value")
	return s


def setup_log_handlers(logger):
	if logger.hasHandlers():
		return

	logger.setLevel(logging.DEBUG)

	# Log formatter
	formatter = logging.Formatter(
		"[%(name)s](%(asctime)s) - %(levelname)s >>> %(message)s")

	# Add a stream handler (logs to terminal)
	stream_handler = logging.StreamHandler()
	stream_handler.setLevel(logging.DEBUG)
	stream_handler.setFormatter(formatter)
	logger.addHandler(stream_handler)

	# Add a rotating file handler (logs to a rotating file)
	rotating_file_handler = handlers.RotatingFileHandler(
		constants.LOGFILE_NAME,
		maxBytes=10000,
		backupCount=5,
		encoding='utf-8')
	rotating_file_handler.setLevel(logging.WARNING)
	rotating_file_handler.setFormatter(formatter)
	logger.addHandler(rotating_file_handler)


def get_logger():
	logger = logging.getLogger("PKG")
	setup_log_handlers(logger)

	return logger
