import os, logging

from flask import Flask, request, current_app
from flask_restful import Api
from flask_jwt_extended import JWTManager
from flask.cli import with_appcontext
from flask_cors import CORS

import click

import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration

from pkg import constants
from pkg.models import db, load_defaults
from pkg.resources.public_key_resource import PublicKeyResource
from pkg.resources.private_key_resource import PrivateKeyResource
from pkg.resources.researcher_resource import ResearchersResource
from pkg.resources.auth_resource import LoginResource
from pkg.resources.auth_resource import LogoutResource
from pkg.resources.college_resource import CollegesResource
from pkg.resources.school_resource import SchoolsResource
from pkg.resources.department_resource import DepartmentsResource


def register_resources(app):
	api = Api(app)

	# Resource URIs
	api.add_resource(PublicKeyResource, "/public_key/<string:algo>")
	api.add_resource(PrivateKeyResource, "/private_key/<string:algo>")
	api.add_resource(ResearchersResource, "/researchers")
	api.add_resource(LoginResource, "/auth/login")
	api.add_resource(LogoutResource, "/auth/logout")
	api.add_resource(CollegesResource, "/colleges")
	api.add_resource(SchoolsResource, "/colleges/<int:college_id>/schools")
	api.add_resource(DepartmentsResource,
		"/schools/<int:school_id>/departments")


def init_with_jwt(app):
	jwt = JWTManager(app)

	@jwt.token_in_blacklist_loader
	def check_if_token_in_blacklist(token):
		from pkg.models.blacklist import Blacklist
		jti = token["jti"]
		blacklist = Blacklist.query.filter_by(token=jti).first()
		return blacklist


def init_app(app):
	# Local and Online Logging
	setup_logging(app)

	init_db(app)

	# JWT Management
	init_with_jwt(app)

	# API Resources registration
	register_resources(app)

	# Enable CORS
	CORS(app, resources={r'/*': {'origins': '*'}})


@click.command("init-db")
@with_appcontext
def create_db():
	db.create_all()
	load_defaults()
	click.echo("Initialized database")


@click.command("clear-db")
@with_appcontext
def drop_db():
	db.drop_all()
	click.echo("Cleared database")


def init_db(app):
	# Database Intialization
	db.init_app(app)

	app.cli.add_command(create_db)
	app.cli.add_command(drop_db)


def file_logging(app):
	app.logger.setLevel(logging.DEBUG)

	# Log formatter
	formatter = logging.Formatter(
		"[%(name)s](%(asctime)s) - %(levelname)s >>> %(message)s")

	# Add a stream handler (logs to terminal)
	stream_handler = logging.StreamHandler()
	stream_handler.setLevel(logging.DEBUG)
	stream_handler.setFormatter(formatter)
	app.logger.addHandler(stream_handler)

	# Add a rotating file handler (logs to a rotating file)
	rotating_file_handler = logging.handlers.RotatingFileHandler(
		constants.LOGFILE_NAME,
		maxBytes=10000,
		backupCount=5,
		encoding='utf-8')
	rotating_file_handler.setLevel(logging.WARNING)
	rotating_file_handler.setFormatter(formatter)
	app.logger.addHandler(rotating_file_handler)


def sentry_logging(app):
	if not app.debug:
		app.sentry = sentry_sdk.init(
			dsn=constants.SENTRY_DSN,
			integrations=[FlaskIntegration()]
		)


def setup_logging(app):
	file_logging(app)
	sentry_logging(app)


def get_config_type():
    return os.environ.get(
    	constants.APP_CONFIG_ENV_VAR,
    	constants.PRODUCTION_CONFIG_VAR
	)


def config_app(app):
	config_type = get_config_type()

	# Possible configurations
	configs = {
		constants.DEVELOPMENT_CONFIG_VAR: "pkg.config.DevelopmentConfig",
		constants.PRODUCTION_CONFIG_VAR: "pkg.config.ProductionConfig"
	}

	app.config.from_object(configs[config_type])

	config_file_path = constants.APP_CONFIG_FILE_PATH

	if config_file_path and os.path.exists(config_file_path):
		app.config.from_pyfile(config_file_path)


def create_app(test_config=None):
	app = Flask(__name__)

	if test_config:
		app.config.from_mapping(test_config)
	else:
		config_app(app)

	init_app(app)

	return app
