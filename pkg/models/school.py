from . import db


class School(db.Model):
	__tablename__ = "school"

	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(100), unique=True, nullable=False)

	college_id = db.Column(db.Integer, db.ForeignKey("college.id"),
		nullable=False)

	departments = db.relationship("Department",
		backref="school", lazy=True)

	def __repr__(self):
		return "School<name={}>".format(self.name)
