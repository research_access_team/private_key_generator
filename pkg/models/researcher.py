from werkzeug.security import generate_password_hash, check_password_hash

from . import db


class Researcher(db.Model):
	__tablename__ = "researcher"

	# Researcher info
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(100), nullable=False)
	email = db.Column(db.String(256), nullable=False, unique=True)
	password = db.Column(db.String(256), nullable=False)

	# Department info
	department_id = db.Column(
		db.Integer, db.ForeignKey("department.id"), nullable=False)

	def __repr__(self):
		return "<Researcher name={}, department={}>".format(
			self.name, self.department)

	@staticmethod
	def hash_password(password):
		return generate_password_hash(password)

	def verify_password(self, password):
		return check_password_hash(self.password, password)
