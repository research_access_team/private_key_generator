institution_levels = {
	"university": {
		"COPAS": {
			"School of Computing and Information Technology": [
				"Computing",
				"Information Technology",
			],
			"School of Mathematical Sciences": [
				"Statistics and Actuarial Sciences",
				"Pure and Applied Mathematics",
			],
			"School of Biological Sciences": [
				"Zoology",
				"Botany",
			],
			"School of Physical Sciences": [
				"Physics",
				"Chemistry",
			],
		},
		"COHES": {
			"School of Nursing": [
				"General Nursing",
				"Community Health and research",
			],
			"School of Medicine": [
				"Child Health and paediatrics",
				"Clinical Medicine",
				"Critical Care and Anaesthesiology",
				"Human Anatomy",
				"Human Pathology",
				"Internal Medicine and Therapeutics",
				"Medicine Physiology",
				"Obstetrics and Gynaecology",
				"Psychiatry",
				"Radiology, Imaging and Radiotherapy",
				"Rehabilitative Sciences",
				"Surgery",
			],
			"School of Pharmacy":[
				"Pharmacy",
			],
			"School of public Health":[
				"Enviromental Health and Disease Control",
				"Community Health and Development",
				"Health Records and Information Management",
			],
			"School of Biomedical Sciences":[
				"Biochemistry",
				"Medical laboratory Sciences",
				"Medical Microbiology",
			],
		},
		"COHRED": {
			"School of Business": [
				"Business Administration",
				"Economics, Accounting and finance",
			],
			"School of Entrepreneurship, Procurement and Management": [
				"Entrepreneurship, Technology, leadership and Management",
				"Procurement and logistics",
			],
			"School of Communication and Development Studies": [
				"Media Technology and Applied Communication",
				"Development Studies",
				"Center for foreign languages and linguistics",
			],
		},
		"COETEC": {
			"School of Architecture and Building Sciences": [
				"Architecture",
				"Landscape Architecture",
				"Construction Management",
			],
			"School of Mechanical, Manufacturing and Materials Engineering": [
				"Mechanical Engineering",
				"Mechatronic Engineering",
				"Marine Engineering and Maritime operations",
				"Mining Materials and Petroleum Engineering",
			],
			"School of Electrical, Electronic and Information Engineering": [
				"Electrical and Electronic Engineering",
				"Telecommunication and Information Engineering",
			],
			"School of Civil, Enviromental and Geospatial Engineering": [
				"Civil, Construction and Environmental Engineering",
				"Geomatic Engineering and Geospatial Information Systems",
			],
			"School of Biosystems and Enviromental Engineering": [
				"Soil, Water and Environmental Engineering",
				"Agricultural and Biosystems Engineering",
			],
		},
		"COANRE":{
			"School of Agriculture and Environmental Sciences": [
				"Horticulture and Food Security",
				"Landscape and Environmental Sciences",
				"Agricultural and Resource Economics",
			],
			"School of Food and Nutrition Sciences": [
				"Food Science and Technology",
				"Human Nutrition Sciences",
			],
			"School of Natural Resources and Animal Sciences": [
				"Land Resources, Planning and Management",
				"Animal Sciences",
			],
		},
	}
}