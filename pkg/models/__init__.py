from flask import current_app
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

from pkg.models.researcher import Researcher
from pkg.models.college import College
from pkg.models.school import School
from pkg.models.department import Department
from pkg.models.blacklist import Blacklist


def load_institution_levels():
	from pkg.models.defaults import institution_levels

	try:
		for college_name in institution_levels["university"].keys():
			college = College(name=college_name)
			db.session.add(college)

			for school_name in institution_levels["university"][college_name].keys():
				school = School(name=school_name)
				school.college = college
				db.session.add(school)

				for department_name in institution_levels["university"][college_name][school_name]:
					department = Department(name=department_name)
					department.school = school
					db.session.add(department)
	
		db.session.commit()
	except Exception as e:
		current_app.logger.error(e)


def load_defaults():
	load_institution_levels()
