from . import db


class College(db.Model):
	__tablename__ = "college"

	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(100), unique=True, nullable=False)

	schools = db.relationship("School",
		backref="college", lazy=True)

	def __repr__(self):
		return "College<name={}>".format(self.name)
