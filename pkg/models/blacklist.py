from pkg.models import db


class Blacklist(db.Model):
	__tablename__ = "blacklist"

	id = db.Column(db.Integer, primary_key=True)
	token = db.Column(db.String(256), nullable=False, unique=True)
