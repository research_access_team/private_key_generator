from . import db


class Department(db.Model):
	__tablename__ = "department"

	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(100), unique=True, nullable=False)

	school_id = db.Column(db.Integer, db.ForeignKey("school.id"),
		nullable=False)

	researchers = db.relationship("Researcher",
		backref="department", lazy=True)

	def __repr__(self):
		return "Department<name={}>".format(self.name)
