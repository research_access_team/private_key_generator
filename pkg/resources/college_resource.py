from flask_restful import reqparse, fields, marshal, Resource

from pkg.models.college import College


class CollegesResource(Resource):

	def get(self):
		colleges = College.query.all()

		return {"colleges": [{"id": college.id, "name": college.name} \
				for college in colleges]}, 200
