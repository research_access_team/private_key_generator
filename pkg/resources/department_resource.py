from flask_restful import Resource

from pkg.models.school import School


class DepartmentsResource(Resource):

	def get(self, school_id):
		school = School.query.get(school_id)

		if not school:
			return {}, 404

		return {"departments": [
					{"id": department.id, "name": department.name} \
				for department in school.departments]}, 200

