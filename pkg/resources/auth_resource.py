from flask import current_app
from flask_restful import Resource, reqparse
from flask_jwt_extended import (create_access_token,
		jwt_required, get_raw_jwt)

from pkg.utils import get_logger, non_empty_string

from pkg.models import db
from pkg.models.researcher import Researcher
from pkg.models.blacklist import Blacklist


class LoginResource(Resource):

	def __init__(self):
		self.logger = get_logger()
	
	def post(self):
		parse = reqparse.RequestParser()
		parse.add_argument("email", required=True, type=non_empty_string)
		parse.add_argument("password", required=True, type=non_empty_string)

		args = parse.parse_args()

		email = args["email"]
		password = args["password"]

		# Check if user exists
		researcher = Researcher.query.filter_by(email=email).first()
		if not researcher:
			return {}, 404

		# Check if password is valid
		if not researcher.verify_password(password):
			return {}, 401

		token = create_access_token(researcher.email)

		return {"name": researcher.name,
				"token": token,
				"college": researcher.department.school.college.name,
				"school": researcher.department.school.name,
				"department": researcher.department.name}, 200


class LogoutResource(Resource):

	@jwt_required
	def get(self):
		jwt = get_raw_jwt()

		try:
			blacklist = Blacklist()
			blacklist.token = jwt["jti"]
			db.session.add(blacklist)
			db.session.commit()

			return {}, 200
		except Exception as e:
			current_app.logger.error(e)

