from flask import current_app
from flask_restful import reqparse, fields, marshal, Resource
from flask_jwt_extended import create_access_token

from pkg.models import db
from pkg.models.researcher import Researcher
from pkg.models.college import College
from pkg.models.school import School
from pkg.models.department import Department
from pkg.utils import non_empty_string, get_logger


class ResearchersResource(Resource):

	def __init__(self):
		self.logger = get_logger()

	@staticmethod
	def mk_post_parser(required_fields, optional_fields=None):
		parser = reqparse.RequestParser()
		for field in required_fields:
			parser.add_argument(field, required=True,
				nullable=False, type=non_empty_string)
		return parser

	def post(self):
		# Expected fields
		required_fields = ["name", "email", "password", "department_id"]

		# Parsers for the fields
		parser = self.__class__.mk_post_parser(required_fields)

		# Enforce checks on fields using parsers
		args = parser.parse_args()

		try:
			exisiting_researcher = Researcher.query.filter_by(
				email=args["email"]).first()
			if exisiting_researcher:
				return {}, 409

			# Go ahead and create the object/row
			researcher = Researcher()
			researcher.name = args["name"]
			researcher.email = args["email"]
			# IMPORTANT: Hash password before storing it
			researcher.password = Researcher.hash_password(args["password"])
			researcher.department = Department.query.get(args["department_id"])

			db.session.add(researcher)
			db.session.commit()
			token = create_access_token(researcher.email)
		except Exception as e:
			current_app.logger.error(e)
			return {}, 500

		return {"name": args["name"],
				"token": token,
				"college": researcher.department.school.college.name,
				"school": researcher.department.school.name,
				"department": researcher.department.name}, 201
