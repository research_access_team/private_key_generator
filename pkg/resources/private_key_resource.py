from flask_restful import reqparse, fields, marshal, Resource
from flask_jwt_extended import jwt_required, get_raw_jwt

from pkg.core.pkg import PKG
from pkg.utils import non_empty_string


class PrivateKeyResource(Resource):

	ecc_fields = {}

	rsa_fields = {"n": fields.String}

	algorithm_fields = {
		"ecc": fields.Nested(ecc_fields),
		"rsa": fields.Nested(rsa_fields)
	}

	fields = {
		"priv_key": fields.String,
		"public_params": fields.Nested(algorithm_fields)
	}

	@jwt_required
	def post(self, algo):
		parser = reqparse.RequestParser()
		parser.add_argument("id", required=True, type=non_empty_string)
		args = parser.parse_args()

		recipient_id = args["id"]

		jwt = get_raw_jwt()

		if not recipient_id == jwt["identity"]:
			return {}, 401

		private_key_generator = PKG(algo)
		private_key = private_key_generator.get_private_key(recipient_id)

		public_params = private_key_generator.get_public_params()

		return marshal(
			{
				"priv_key": str(private_key),
				"public_params": {
					"ecc": {}, #TODO
					"rsa": {
						"n": str(public_params["n"])
					}
				}
			}, self.fields), 200
