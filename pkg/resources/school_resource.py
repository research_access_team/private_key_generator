from flask_restful import Resource

from pkg.models.college import College


class SchoolsResource(Resource):

	def get(self, college_id):
		college = College.query.get(college_id)

		if not college:
			return {}, 404

		return {"schools": [{"id": school.id, "name": school.name} \
				for school in college.schools]}, 200
