from flask import g, current_app
from flask_restful import reqparse, fields, marshal, Resource
from flask_restful.representations.json import output_json
from flask_jwt_extended import jwt_required

from pkg.core.pkg import PKG
from pkg.utils import non_empty_string


class PublicKeyResource(Resource):

	ecc_fields = {}

	rsa_fields = { "n": fields.Integer }

	algo_fields = {
		"ecc": fields.Nested(ecc_fields),
		"rsa": fields.Nested(rsa_fields)
	}

	fields = {
		"pub_key": fields.Integer,
		"public_params": fields.Nested(algo_fields)
	}

	@jwt_required
	def post(self, algo):
		parser = reqparse.RequestParser()
		parser.add_argument("id", required=True, type=non_empty_string)
		args = parser.parse_args()

		recipient_id = args["id"]

		private_key_generator = PKG(algo)
		public_key = private_key_generator.get_public_key_based_on_id(recipient_id)

		public_params = private_key_generator.get_public_params()

		return {"pub_key": str(public_key),
				"public_params": {
					"ecc": {}, #TODO
					"rsa": {
						"n": str(public_params["n"])
					}
				}
				}, 200
