from random import randint
import math


bit_length = 1024

class RSA(object):

	def __init__(self):
		# self.p = RSA.generate_big_prime(bit_length)
		# self.q = RSA.generate_big_prime(bit_length)

		# TEMPORARILY hardcode large primes
		self.p = 129249436065370439991078887294552452568007985187596865041006442848504813939213528035876363243423110778276712641478720537997638478017059720088837745881983272846833409658473514085274397899132874260388093246424573488093166285686414069523398609417559720341277714469388208103113087531241792667208280342664814355201
		self.q = 143922886601600875927950786133357558772046883834066085731163711861203221613223415573592640199556945888828175276739011602879012041716864901165650615376989135894824216252073780859569727147125462061047194718267321967440783599773265246852319679075899711025064036476597941260777361152072718813638356662122545662609

		self.n = self.p * self.q

	def is_prime(num, test_count):
	    if num == 1:
	        return False
	    if test_count >= num:
	        test_count = num - 1
	    for x in range(test_count):
	        val = randint(1, num - 1)
	        if pow(val, num-1, num) != 1:
	            return False
	    return True

	def generate_big_prime(n):
	    found_prime = False
	    while not found_prime:
	        p = randint(2**(n-1), 2**n)
	        if RSA.is_prime(p, 1000):
	            return p

	def get_public_key_from_id(self, recipient_id):
		self.e = hash(recipient_id)
		self.phi = (self.p - 1) * (self.q - 1)

		gcd_phi_e = math.gcd(self.phi, self.e)

		while gcd_phi_e != 1:
			self.e = self.e // gcd_phi_e
			gcd_phi_e = math.gcd(self.phi, self.e)

		return self.e

	def get_private_key(self, recipient_id):
		e = self.get_public_key_from_id(recipient_id)
		self.d = (self.extended_euclidean_algo(e, ((self.p -1) * (self.q - 1))))[1]
		return self.d

	def extended_euclidean_algo(self, a, b):
		prev_x, x = 1, 0
		prev_y, y = 0, 1

		while b:
			q = a // b

			x, prev_x = (prev_x - (q*x)), x
			y, prev_y = (prev_y - (q*y)), y

			a, b = b, a % b

		return a, prev_x, prev_y


	def get_public_params(self):
		return {"n": self.n}

