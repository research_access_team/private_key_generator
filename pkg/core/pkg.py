from pkg.core.rsa import RSA


class PKG(object):

	def __init__(self, algo):
		if algo.lower() in ["rsa", "ecc"]:
			if algo == "rsa":
				self.algo = RSA()
			elif algo == "ecc":
				self.algo = None # TODO: ECC implementation of IBE
		else:
			raise Exception("Algorithm not recognized, must be either RSA or ECC")

	def get_public_key_based_on_id(self, recipient_id):
		recipient_public_key = self.algo.get_public_key_from_id(recipient_id)
		return recipient_public_key

	def get_private_key(self, recipient_id):
		private_key = self.algo.get_private_key(recipient_id)
		return private_key

	def get_public_params(self):
		return self.algo.get_public_params()
