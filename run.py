import os

from pkg import create_app


# Use port 5000 by default
port = int(os.environ.get("PORT", 5000))


if __name__ == "__main__":
	app = create_app()
	app.run(host="0.0.0.0", port=port)
